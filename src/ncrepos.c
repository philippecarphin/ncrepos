#include <ncurses.h>
#include "ncrepos.h"
#include "yaml.h"

static WINDOW *my_ncurses_init();

int print_repo_result(WINDOW *report_win, int y, int x, const char *repo_path, struct RepoInfo *ri, int fetch_err)
{
    mvwprintw(report_win, y, x, " ");
    int color;
    if(ri->state.diverged || ri->state.ahead || ri->state.behind){
        wattron(report_win, COLOR_PAIR(5)|A_BOLD);
    }
    if(ri->state.diverged){
        wprintw(report_win, "(DIV)    ");
    } else if (ri->state.ahead){
        wprintw(report_win, "(AHEAD)  ");
    } else if (ri->state.behind){
        wprintw(report_win, "(BEHIND) ");
    } else {
        wprintw(report_win, "         ");
    }
    wattroff(report_win, COLOR_PAIR(5)|A_BOLD);

    if(fetch_err){
        color = 4;
    } else if(ri->state.untracked_files){
        color = 1;
    } else if (ri->state.dirty) {
        color = 2;
    } else {
        color = 3;
    }
    wattron(report_win, COLOR_PAIR(color)|A_BOLD);
    wprintw(report_win, "%s",repo_path);
    wattroff(report_win, COLOR_PAIR(color)|A_BOLD);
    wrefresh(report_win);
    return 0;
}

int ncrepos_main(int argc, char **argv)
{

    WINDOW *main_window = my_ncurses_init();
    if(main_window == NULL){
        goto end;
    }
    int n_rows = 0;
    int n_cols = 0;
    getmaxyx(main_window, n_rows, n_cols);

    int fetch_repos = 1;
    if(argc >= 2 && (strcmp(argv[1], "-no-fetch") == 0)){
        fetch_repos = 0;
    }

    WINDOW *title_win = newwin(8, n_cols-2,0,1);
    box(title_win, 0,0);
    WINDOW *report_win = newwin(n_rows-2 - 11, n_cols-2, 11 ,1);
    box(report_win, 0,0);
    WINDOW *status_win = newwin(3,n_cols - 2,8,1);
    box(status_win, 0,0);
    refresh();

    char *repo_paths[1000];
    int nb_repo_paths = 0;
    int err = 0;

    char *home = getenv("HOME");
    char yaml_path[1000];
    snprintf(yaml_path, 1000, "%s/.config/%s", home, "repos.yml");
    parse_repos_yaml_path(yaml_path, repo_paths, &nb_repo_paths);

    mvwprintw(title_win, 1,2, "Welcome to %s a tool to keep track of git repos", argv[0]);
    mvwprintw(title_win, 3,2, "Scanned yaml file '%s'", yaml_path);
    mvwprintw(title_win, 5,5, "Found %d repos.", nb_repo_paths);
    wrefresh(title_win);

    for(int i = 0; i < nb_repo_paths ; i++){
        mvwprintw(report_win, 2+i, 1, " ");
        wprintw(report_win, "         ");
        wprintw(report_win, "%s",repo_paths[i]);
    }
    refresh();
    wrefresh(report_win);

    char *info_str = fetch_repos ? "Fetching and acquingig" : "Acquiring";
    int nb_errors = 0;
    for(int i = 0; i < nb_repo_paths ; i++){
        struct RepoInfo ri;
        // forgive me for clearing like this
        mvwprintw(status_win, 0,3," %s git info for ", info_str);
        mvwprintw(status_win, 1, 5, "                                                                               ");
        mvwprintw(status_win, 1, 5, "'%s'", repo_paths[i]);
        wrefresh(status_win);

        int fetch_status = 0;
        if(fetch_repos){
            fetch_status = git_fetch(repo_paths[i]);
            if(fetch_status != 0){
                mvwprintw(status_win,1,1, "Error for repo #%d : %s", i, repo_paths[i], fetch_status);
                nb_errors++;
            }
        }

        int err = get_complete_info(repo_paths[i], &ri);
        if(err){
            mvwprintw(status_win,1,1, "Error for repo #%d : %s", i, repo_paths[i]);
            nb_errors++;
        }
        refresh();

        print_repo_result(report_win, 2+i, 1, repo_paths[i], &ri, fetch_status);

        free(repo_paths[i]);
    }

    mvprintw(12 + nb_repo_paths + 2, 5, "DONE, press two keys to quit");
    getch();
    getch();

end:
    endwin();
    return err;
}
WINDOW *my_ncurses_init(){

    WINDOW* main_window = initscr();
    if(main_window == NULL){
        return NULL;
    }
    int err = 0;

    err = noecho();
    if(err){
        return NULL;
    }

    err = raw();
    if(err){
        return NULL;
    }

    err = keypad(stdscr, TRUE);
    if(err){
        printf("ERROR : Failed to enable keypad F1, F2, Arrows, ...\n");
        return NULL;
    }

    int prev_state = curs_set(0);
    if(prev_state == ERR){
        printf("ERROR : Failed to disable curson\n");
        return NULL;
    }

    start_color();
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_YELLOW, COLOR_BLACK);
    init_pair(3, COLOR_GREEN, COLOR_BLACK);
    init_pair(4, COLOR_WHITE, COLOR_RED);
    init_pair(5, COLOR_MAGENTA, COLOR_BLACK);

    return main_window;
}



