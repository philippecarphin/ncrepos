#include "ncrepos.h"
#include "popenRWE.h"
#include <stdlib.h>
#include <string.h>
#include <libgen.h>

int git_fetch(const char *path){
    if(path == NULL){
        return 1;
    }
    char fetch_cmd[1000];
    snprintf(fetch_cmd, 1000, "cd %s && git fetch >/dev/null 2>&1", path);
    int result = system(fetch_cmd);
    return WEXITSTATUS(result);
}

// I almost added a boolean parameter for fetching LOOOOOL
int get_modified_state(const char *path, struct RepoState *rs)
{
    if(path == NULL){
        return 1;
    }

    char status_cmd[1000];
    int pipes[3];
    snprintf(status_cmd, 1000, "cd %s && git status 2>&1", path);
    int pid = popenRWE(pipes, status_cmd);

    char output[10000];
    char *out_ptr = output;
    char buff[100];
    int nb_read;

    while((nb_read = read(pipes[1], buff, sizeof(buff))) != 0){
        memcpy(out_ptr, buff, nb_read);
        out_ptr += nb_read;
    }

    *out_ptr = '\0';

	// TODO Use two of these to check for staged and unstaged
    // cmd := r.gitCommand("diff", "--no-ext-diff", "--quiet", "--exit-code")
    // With --staged to check for staged changes
    // Without to check for unstaged changes
    rs->behind = (strstr(output, "Your branch is behind") != NULL);
    rs->ahead = (strstr(output, "Your branch is ahead") != NULL);
    rs->diverged = (strstr(output, "different commits each, respectively.") != NULL);
    rs->staged_changes = (strstr(output, "Changes to be committed:") != NULL);
    rs->unstaged_changes = (strstr(output, "Changes not staged for commit:") != NULL);
    rs->dirty = rs->unstaged_changes | rs->staged_changes;

    // TODO Use for untracked (untracked == empty output) r.gitCommand("ls-files", r.Path, "--others", "--exclude-standard")
    rs->untracked_files = (strstr(output, "Untracked files:") != NULL);

    int pretval;
out_close:
    pretval = pcloseRWE(pid, pipes);
    if(pretval != 0){
        return pretval;
    }
    return 0;
}

int get_time_since_last_commit(const char *path, long unsigned int *last_commit_timestamp)
{
    char cmd[1000];
    snprintf(cmd, 1000, "cd %s && git log --pretty=format:%%at 2>&1", path);
    FILE *fp = popen(cmd, "r");
    if(fp == NULL){
        return 1;
    }
    int unix_timestamp;

    char buff[100];
    // I think I can just do sscanf here
    while(fgets(buff, sizeof(buff), fp) != NULL){
        sscanf(buff, "%u", &unix_timestamp);
    }

    *last_commit_timestamp = unix_timestamp;

    return 0;
}

int get_complete_info(const char *path, struct RepoInfo* repo_info)
{
    snprintf(repo_info->config.path, 1000, "%s", path);
    snprintf(repo_info->config.name, 100, "%s", basename((char*)path));
    snprintf(repo_info->config.short_name, 100, "%s", basename((char*)path));
    repo_info->config.fetch = 1;
    snprintf(repo_info->config.comment, 100, "check");

    int err = get_modified_state(path, &(repo_info->state));
    if(err){
        return err;
    }

    err = get_time_since_last_commit(path, &(repo_info->state.time_since_last_commit));
    if(err){
        return err;
    }

    return 0;
}
