#ifndef _NCURSES_DEMO_H
#define _NCURSES_DEMO_H

#include <ncurses.h>

int ncrepos_main(int argc, char **argv);
// Structs for describing the state of a repo
struct RepoState {
    int dirty;
    int untracked_files;
    long unsigned int time_since_last_commit;
    int behind;
    int unstaged_changes;
    int staged_changes;
    int ahead;
    int diverged;
};
struct RepoConfig {
    char path[1000];
    char name[100];
    char short_name[100];
    int fetch;
    char comment[100];
};
struct RepoInfo {
    struct RepoConfig config;
    struct RepoState state;
};

int get_complete_info(const char *path, struct RepoInfo* repo_info);
int git_fetch(const char *path);


// Program configuration
struct Config {
    int color;
    struct RepoConfig defaults;
};

// File containting the list of repos and configurations
struct RepoFile {
    struct RepoConfig repos[1000];
    struct Config config;
};

// Program arguments
struct Args {
    char command[1000];
    char path[1000];
    char name[1000];
    int generate_config;
};

int parse_repos_yaml_path(const char *yaml_path, char **repo_paths, int *nb_repo_paths);
#endif
