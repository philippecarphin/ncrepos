#include "yaml.h"

int parse_repos_yaml_file(FILE *file, char **repo_paths, int *nb_repo_paths);
int parse_repos_yaml_path(const char *yaml_path, char **repo_paths, int *nb_repo_paths)
{
    int ret = 0;
    yaml_parser_t parser;
    yaml_parser_initialize(&parser);

    FILE *file = fopen(yaml_path, "rb");
    if(file == NULL){
        return 1;
    }

    int err = parse_repos_yaml_file(file, repo_paths, nb_repo_paths);
    if(err){
        ret = err;
        goto out_file;
    }

out_file:
    fclose(file);
out:
    return ret;
}

// FROM  https://gist.github.com/meffie/89d106a86b81c579c2b2a1895ffa18b0
#define INDENT "  "
#define STRVAL(x) ((x) ? (char*)(x) : "")

void indent(int level)
{
    int i;
    for (i = 0; i < level; i++) {
        printf("%s", INDENT);
    }
}

void print_event(yaml_event_t *event)
{
    static int level = 0;

    switch (event->type) {
    case YAML_NO_EVENT:
        indent(level);
        printf("no-event\n");
        break;
    case YAML_STREAM_START_EVENT:
        indent(level++);
        printf("stream-start-event\n");
        break;
    case YAML_STREAM_END_EVENT:
        indent(--level);
        printf("stream-end-event\n");
        break;
    case YAML_DOCUMENT_START_EVENT:
        indent(level++);
        printf("document-start-event\n");
        break;
    case YAML_DOCUMENT_END_EVENT:
        indent(--level);
        printf("document-end-event\n");
        break;
    case YAML_ALIAS_EVENT:
        indent(level);
        printf("alias-event\n");
        break;
    case YAML_SCALAR_EVENT:
        indent(level);
        printf("scalar-event={value=\"%s\", length=%d}\n",
                STRVAL(event->data.scalar.value),
                (int)event->data.scalar.length);
        break;
    case YAML_SEQUENCE_START_EVENT:
        indent(level++);
        printf("sequence-start-event\n");
        break;
    case YAML_SEQUENCE_END_EVENT:
        indent(--level);
        printf("sequence-end-event\n");
        break;
    case YAML_MAPPING_START_EVENT:
        indent(level++);
        printf("mapping-start-event\n");
        break;
    case YAML_MAPPING_END_EVENT:
        indent(--level);
        printf("mapping-end-event\n");
        break;
    }
    if (level < 0) {
        fprintf(stderr, "indentation underflow!\n");
        level = 0;
    }
}

int expect_mapping_start_event(yaml_parser_t *parser){

    yaml_event_t event;
    int status = yaml_parser_parse(parser, &event);
    if (!status){
        return 1;
    }
    int ret = event.type == YAML_MAPPING_START_EVENT;
    yaml_event_delete(&event);
    return ret;
}

int parse_single_repo_mapping(yaml_parser_t *parser, char **repo_paths, int *nb_repo_paths)
{
    // printf("\033[1m%s() START\033[0m\n", __func__);
    int ret = 0;

    for(;;){
        yaml_event_t event;
        int status = yaml_parser_parse(parser, &event);
        if (!status){
            return 1;
        }

        if(event.type == YAML_MAPPING_END_EVENT){
            // printf("\033[1;33m%s()Returning due to YAML_MAPPING_END_EVENT\033[0m\n", __func__);
            yaml_event_delete(&event);
            return 0;
        }

        char *scalar = STRVAL(event.data.scalar.value);
        if(strcmp(scalar, "path") == 0){
            yaml_event_delete(&event);
            int status = yaml_parser_parse(parser, &event);
            if (!status){
                return 1;
            }
            repo_paths[(*nb_repo_paths)++] = strdup(STRVAL(event.data.scalar.value));
            yaml_event_delete(&event);
        } else if(strcmp(scalar, "fetch") == 0){
            // printf("found fetch");
            yaml_event_delete(&event);
            int status = yaml_parser_parse(parser, &event);
            if (!status){
                return 1;
            }
            yaml_event_delete(&event);
        } else if(strcmp(scalar, "shortname") == 0){
            // printf("found shortname");
            yaml_event_delete(&event);
            int status = yaml_parser_parse(parser, &event);
            if (!status){
                return 1;
            }
            yaml_event_delete(&event);
        } else if(strcmp(scalar, "ignore") == 0){
            // printf("found shortname");
            yaml_event_delete(&event);
            int status = yaml_parser_parse(parser, &event);
            if (!status){
                return 1;
            }
            yaml_event_delete(&event);
        } else if(strcmp(scalar, "comment") == 0){
            // printf("found shortname");
            yaml_event_delete(&event);
            int status = yaml_parser_parse(parser, &event);
            if (!status){
                return 1;
            }
            yaml_event_delete(&event);
        } else {
            // printf("Unknown key '%s'\n", scalar);
            yaml_event_delete(&event);
            return 1;
        }
    }

    return ret;
}

int parse_repos_mapping(yaml_parser_t *parser, char **repo_paths, int *nb_repo_paths)
{
    // printf("\033[1m%s() START\033[0m\n", __func__);
    int ret = 0;
    yaml_event_t event;
    if(!expect_mapping_start_event(parser)){
        return 1;
    }

    int depth = 1;
    for(;depth>0;){
        int status = yaml_parser_parse(parser, &event);
        if (!status){
            return 1;
        }

        if(event.type == YAML_MAPPING_END_EVENT){
            break;
        }

        if(event.type != YAML_SCALAR_EVENT){
            print_event(&event);
            return 1;
        }

        char *scalar = STRVAL(event.data.scalar.value);

        yaml_event_delete(&event);
        status = yaml_parser_parse(parser, &event);
        if (!status){
            return 1;
        }

        if(event.type != YAML_MAPPING_START_EVENT){
            return 1;
        }

        int err = parse_single_repo_mapping(parser, repo_paths, nb_repo_paths);
        if(err){
            return 1;
        }

        yaml_event_delete(&event);
    }

    // for(int i = 0; i < *nb_repo_paths ; i++){
    //     printf("\033[1;35mRepo path : %s\033[0m\n", repo_paths[i]);
    //     // free(repo_paths[i]);
    // }

    // printf("%s() END\n", __func__);
    return ret;
}
int parse_repos_yaml_file(FILE *file, char **repo_paths, int *nb_repo_paths)
{
    yaml_parser_t parser;
    yaml_parser_initialize(&parser);
    yaml_parser_set_input_file(&parser, file);

    int depth = 0;
    for(;;){

        yaml_event_t event;
        int status = yaml_parser_parse(&parser, &event);
        if (!status)
            goto error;

        if(event.type == YAML_STREAM_END_EVENT){
            yaml_event_delete(&event);
            break;
        }

        if (event.type == YAML_SCALAR_EVENT){
            char *scalar = STRVAL(event.data.scalar.value);
            // printf("Scalar found at depth %d, %s\n", depth, scalar);
            if(strcmp(STRVAL(event.data.scalar.value), "config") == 0){
                // printf("\033[1mI FOUND THE CONFIG NODE at depth %d\033[0m\n", depth);
                // TODO parse_config_mapping(&parser);
            } else if(strcmp(STRVAL(event.data.scalar.value), "repos") == 0){
                int err = parse_repos_mapping(&parser, repo_paths, nb_repo_paths);
                if(err){
                    goto error;
                }
            }
        } else {
            // print_event(&event);
        }
        yaml_event_delete(&event);
    }

    yaml_parser_delete(&parser);
    return 0;

error:
    // fprintf(stderr, "Failed to parse: %s\n", parser.problem);
    yaml_parser_delete(&parser);
    return 1;
}
