#include "ncrepos.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ncurse_testing.h"
//
// I feel like the tests could set a global string
// and the main function could have
//
// print test start
// run test
// check return value of test and print checkmark or failure info.
//
// It has to be both : I need the output to have the line number where the the failure appears but I can still have the "Info" output like printing something before and after the test.


#define SUCCESS 0
#define FAILURE 1
// Cross mark ✗ : HEX 0xe29c97
// Checkmark ✓  : HEX 0xe29c93
// I could put the unicode char itself in the string but for some
// reason this feels more portable.
#define FAIL printf("\033[31m\xe2\x9c\x97\033[1;37m %s()\033[0m \n%s:%d: fail: \n", __func__, __FILE__, __LINE__)
#define FAIL_PRINT(x) FAIL;putchar('\t');printf((x));putchar('\n')
#define SUCCESS_PRINT printf("\033[32m\xe2\x9c\x93\033[1;37m %s()\033[0m\n", __func__)


int test_test_dir(){
    char *repo = TEST_DIR;
    SUCCESS_PRINT;
    return SUCCESS;
}
int test_get_repo_info_modified(){
    char repo[1000];
    snprintf(repo, sizeof(repo), "%s/%s", TEST_DIR, "dirty_repo");

    struct RepoState rs;
    int err = get_modified_state(repo, &rs);
    if(err){
        FAIL_PRINT("Error calling get_modified_state");
        return FAILURE;
    }

    if(rs.dirty != 1){
        FAIL_PRINT("Repo should be modified");
        return FAILURE;
    }

    SUCCESS_PRINT;
    return SUCCESS;
}

int test_get_repo_info_untracked(){
    char repo[1000];
    snprintf(repo, sizeof(repo), "%s/%s", TEST_DIR, "untracked_repo");

    int modified, untracked;
    struct RepoState rs;
    int err = get_modified_state(repo, &rs);
    if(err){
        FAIL_PRINT("Error calling get_modified_state");
        return FAILURE;
    }

    if(rs.untracked_files != 1){
        FAIL_PRINT("Repo should have untracked files");
        return FAILURE;
    }
    SUCCESS_PRINT;
    return SUCCESS;
}

int test_get_repo_info_clean(){
    char repo[1000];
    snprintf(repo, sizeof(repo), "%s/%s", TEST_DIR, "clean_repo");
    int result = SUCCESS;

    struct RepoState rs;
    int err = get_modified_state(repo, &rs);
    if(err){
        return FAILURE;
    }

    if(rs.untracked_files != 0){
        FAIL_PRINT("Untracked files detected");
        result = FAILURE;
    }
    if(rs.dirty != 0){
        FAIL_PRINT("Modified files detected");
        return FAILURE;
    }
    if(!result) SUCCESS_PRINT;
    return result;
}

int test_get_time_since_last_commit()
{
    char repo[1000];
    snprintf(repo, sizeof(repo), "%s/%s", TEST_DIR, "clean_repo");

    long unsigned int last_commit_timestamp = 0;
    int err = get_time_since_last_commit(repo, &last_commit_timestamp);
    if(err){
        printf("FAIL: %s:%d:\n\tCould not get time since last commit for\n\t'%s'\n", __FILE__, __LINE__,repo);
        return FAILURE;
    }

    if(last_commit_timestamp != 100000){
        FAIL_PRINT("Timestamp of last commit was expected to be 0");
        printf("\tGot %lu\n", last_commit_timestamp);
        return FAILURE;
    }

    SUCCESS_PRINT;
    return SUCCESS;
}

int test_get_complete_info()
{
    char repo[1000];
    snprintf(repo, sizeof(repo), "%s/%s", TEST_DIR, "clean_repo");

    struct RepoInfo repo_info;
    int err = get_complete_info(repo, &repo_info);
    if(err){
        FAIL;
        return FAILURE;
    }

    if(strcmp(repo, repo_info.config.path) != 0){
        FAIL;
        return FAILURE;
    }
    if(strcmp("clean_repo", repo_info.config.name) != 0){
        FAIL;
        return FAILURE;
    }
    if(repo_info.state.dirty || repo_info.state.untracked_files){
        FAIL_PRINT("Incorrect detection of repo state for clean_repo");
        return FAILURE;
    }
    if(repo_info.state.time_since_last_commit != 100000){
        FAIL_PRINT("incorrect time_since_last_commit");
        printf("\tGot %lu\n", repo_info.state.time_since_last_commit);
        return FAILURE;
    }

    SUCCESS_PRINT;
    return SUCCESS;
}

int test_github_gist_demo()
{
    char yaml_path[1000];
    snprintf(yaml_path, sizeof(yaml_path), "%s/%s", TEST_DIR, "repos.yml");


    char *repo_paths[1000];
    int nb_repo_paths = 0;

    int err = parse_repos_yaml_path(yaml_path, repo_paths, &nb_repo_paths);
    if(err){
        FAIL_PRINT("Could not get data from yaml");
        return FAILURE;
    }

    SUCCESS_PRINT;
    return SUCCESS;
}

int test_github_gist_demo_alternate()
{
    char yaml_path[1000];
    snprintf(yaml_path, sizeof(yaml_path), "%s/%s", TEST_DIR, "repos.alternate.yml");
    char *repo_paths[1000];
    int nb_repo_paths = 0;


    int err = parse_repos_yaml_path(yaml_path, repo_paths, &nb_repo_paths);
    if(err){
        FAIL_PRINT("Could not get data from yaml");
        return FAILURE;
    }

    SUCCESS_PRINT;
    return SUCCESS;
}

int main(int argc, char **argv){
    int nb_fail = 0;

    nb_fail += test_test_dir();
    nb_fail += test_get_repo_info_modified();
    nb_fail += test_get_repo_info_untracked();
    nb_fail += test_get_repo_info_clean();
    nb_fail += test_get_time_since_last_commit();
    nb_fail += test_get_complete_info();
    nb_fail += test_github_gist_demo();
    // nb_fail += test_github_gist_demo_alternate();

    return nb_fail;
}
