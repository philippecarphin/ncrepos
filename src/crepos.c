#include <stdio.h>
#include <stdlib.h>
#include "ncrepos.h"
#include "yaml.h"

int print_repo_result_term(const char *repo_path, struct RepoInfo *ri, int fetch_err);
int main(int argc, char **argv)
{
    int fetch_repos = 1;
    int show_recent_commits = 0;
    if(argc >= 2 && (strcmp(argv[1], "-no-fetch") == 0)){
        fetch_repos = 0;
    }
    if(argc >= 2 && (strcmp(argv[1], "-recent") == 0)){
        show_recent_commits = 1;
    }

    char *repo_paths[1000];
    int nb_repo_paths = 0;
    int err = 0;

    char *home = getenv("HOME");
    char yaml_path[1000];
    snprintf(yaml_path, 1000, "%s/.config/%s", home, "repos.yml");
    parse_repos_yaml_path(yaml_path, repo_paths, &nb_repo_paths);
    if(nb_repo_paths > 1000){
        printf("There were more than 1000 repos.  Some data was likely written passed the bounds of the repo_paths array which could lead to a segfault at best and weird behavior at worst\n");
        return 1;
    }

    printf("Welcome to %s a tool to keep track of git repos\n", argv[0]);
    printf("Scanned yaml file '%s'\n", yaml_path);
    printf("Found %d repos.\n", nb_repo_paths);
    int nb_errors = 0;
    for(int i = 0; i < nb_repo_paths ; i++){
        if(show_recent_commits){
            char cmd[1000];
            snprintf(cmd, 1000, "cd %s && git recent", repo_paths[i]);
            printf("'%s'\n", cmd);
            system(cmd);
            continue;
        }

        struct RepoInfo ri;
        printf("      \033[33m==>\033[0m    %s\n\033[1A", repo_paths[i]);
        int fetch_status = 0;
        if(fetch_repos){
            fetch_status = git_fetch(repo_paths[i]);
            if(fetch_status != 0){
                printf("Error for repo #%d : %s : %d\n", i, repo_paths[i], fetch_status);
                nb_errors++;
            }
        }
        int err = get_complete_info(repo_paths[i], &ri);
        if(err){
            printf("Error for repo #%d : %s\n", i, repo_paths[i]);
            nb_errors++;
        }

        print_repo_result_term(repo_paths[i], &ri, fetch_status);

        free(repo_paths[i]);
    }
    return 0;
}

int print_repo_result_term(const char *repo_path, struct RepoInfo *ri, int fetch_err)
{
    printf(" ");
    int color;
    if(ri->state.diverged){
        printf("\033[1;35m(DIV)    \033[0m");
    } else if (ri->state.ahead){
        printf("\033[1;35m(AHEAD)  \033[0m");
    } else if (ri->state.behind){
        printf("\033[1;35m(BEHIND) \033[0m");
    } else {
        printf("         ");
    }

    if(fetch_err){
        color = 4;
        printf("\033[1;37;41m");
    } else if(ri->state.untracked_files){
        printf("\033[1;31m");
    } else if (ri->state.dirty) {
        printf("\033[1;33m");
    } else {
        printf("\033[1;32m");
    }

    printf("   %s\033[0m\n", repo_path);

    return 0;
}

