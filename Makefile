include color-makefile/colors.mk

CFLAGS += -I ./libyaml/include -MMD -g

LDFLAGS += -L build_yaml -L$(build_dir) -L /usr/lib/x86_64-linux-gnu
LDLIBS += -l yaml -lcurses -lform
src_dir = src
build_dir = build_make
PREFIX ?= /usr/local

all: yaml $(build_dir)/ncrepos $(build_dir)/crepos
#
# Main executable
#
all_src = $(wildcard $(src_dir)/*.c)

src = $(wildcard $(src_dir)/*.c)
MAIN_SRC = $(src_dir)/main.c $(src_dir)/crepos.c
TEST_SRC = $(src_dir)/test.c
NO_MAINS = $(filter-out $(MAIN_SRC),$(SRC))
LIB_SRC = $(filter-out $(TEST_SRC),$(NO_MAINS))
LIB_OBJ = $(patsubst $(src_dir)/%.c,$(build_dir)/%.o,$(LIB_SRC))


LIBOBJ = $(build_dir)/ncrepos.o $(build_dir)/popenRWE.o $(build_dir)/get_git_info.o $(build_dir)/repos_yaml_file.o
$(build_dir)/ncrepos: $(build_dir)/main.o $(build_dir)/libncrepos.a
	$(call make_echo_link_c_executable)
	$(at) gcc $(LDFLAGS) $^ -o $@ $(LDLIBS)
	$(at) echo "Built target $@"
$(build_dir)/crepos: $(build_dir)/crepos.o $(build_dir)/libncrepos.a
	$(call make_echo_link_c_executable)
	$(at) gcc $(LDFLAGS) $^ -o $@ $(LDLIBS)
	$(at) echo "Built target $@"

$(build_dir)/libncrepos.a: $(LIBOBJ)
	$(call make_echo_link_static_library)
	$(at) ar -crs $@ $^

install:
	$(call make_echo_color_bold,cyan,Installing project)
	mkdir -p $(PREFIX)/bin
	install $(build_dir)/crepos $(PREFIX)/bin
	install $(build_dir)/ncrepos $(PREFIX)/bin

#
# Testing targets
#
check: yaml check_unittest

# check_unittest targets
check_unittest:$(build_dir)/unittest git_repos
	$(call make_echo_run_test,Running unit test executable $<)
	$(at) ./$<
	$(call success)
$(build_dir)/unittest: $(build_dir)/test.o $(build_dir)/libncrepos.a
	$(call make_echo_link_c_executable)
	$(at) gcc $(LDFLAGS) $^ -o $@ $(LDLIBS)
	$(at) echo "Built target $@"
$(build_dir)/test.o:$(src_dir)/test.c
	$(call make_echo_build_c_object)
	$(at) gcc -c $(CFLAGS) -DTEST_DIR=\"$(PWD)/$(build_dir)/git_repos\" $< -o $@

#
# COMPILATION RULES
#
$(build_dir)/%.o: src/%.c
	$(call make_echo_build_c_object)
	$(at) gcc -c $(CFLAGS) $< -o $@

#
# Clean
#
clean:
	$(at) rm -rf ./$(build_dir)/*
	$(at) rm -rf ./build_make

# EXTERNAL THING YAML
# The real build is handled by CMake
#
yaml:$(build_dir)/libyaml.a
$(build_dir)/libyaml.a: Makefile
	$(call make_echo_color_bold,blue,Building yaml using real CMake project)
	$(at) mkdir -p $(build_dir) ; cd $(build_dir) ; cmake $(PWD)/libyaml ; make --no-print-directory
	$(call success)
git_repos:
	$(call make_echo_generate_file)
	$(at) mkdir -p $(build_dir)/git_repos ; cd $(build_dir)/git_repos ; cmake $(PWD) ; make --no-print-directory git_repos

-include *.d
